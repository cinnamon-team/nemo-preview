Source: nemo-preview
Section: utils
Priority: optional
Maintainer: Debian Cinnamon Team <debian-cinnamon@lists.debian.org>
Uploaders:
 Joshua Peisach <itzswirlz2020@outlook.com>,
 Norbert Preining <norbert@preining.info>,
Build-Depends: debhelper-compat (= 13),
 gir1.2-xreader,
 gnome-common,
 gnome-pkg-tools (>= 0.11),
 gobject-introspection (>= 0.9.2),
 intltool,
 libcjs-dev,
 libclutter-1.0-dev (>= 1.11.4),
 libclutter-gst-3.0-dev,
 libclutter-gtk-1.0-dev (>= 1.0.1),
 libfreetype6-dev,
 libgirepository1.0-dev (>= 0.9.2),
 libglib2.0-dev (>= 2.37.3),
 libgstreamer1.0-dev,
 libgtk-3-dev (>= 3.5.12),
 libgtksourceview-3.0-dev,
 libmusicbrainz5-dev,
 libwebkit2gtk-4.0-dev,
 libxreaderview-dev,
 libxreaderdocument-dev
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://community.linuxmint.com/software/view/nemo-preview
Vcs-Browser: https://salsa.debian.org/cinnamon-team/nemo-preview
Vcs-Git: https://salsa.debian.org/cinnamon-team/nemo-preview.git

Package: nemo-preview
Architecture: any
Depends: cjs (>= 3.9.0),
         gir1.2-clutter-gst-3.0,
         gir1.2-xreader,
         gir1.2-gtkclutter-1.0,
         gir1.2-webkit2-4.0,
         gstreamer1.0-plugins-good,
         libfreetype6,
         libgirepository-1.0-1,
         libgtksourceview-3.0-1,
         libmusicbrainz5-2,
         libpango-1.0-0,
         nemo (>= 3.9.0)
Description: nemo-preview is a quick previewer for nemo
 Nemo-preview is a GtkClutter and Javascript-based quick previewer
 for Nemo, the Cinnamon desktop file manager.
 Nemo-preview is a DBus-activated service. It is capable of previewing
 documents, PDFs, sound and video files (using Gstreamer),
 some text files, and possibly others in the future.
 .
 To activate the preview, left-click the file and hit space.
 The preview can be closed by hitting space again, or escape.
